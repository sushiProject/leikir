import { AppRoute } from './types';
import { Contacts } from './containers/contacts';
import { CreateContact } from './containers/createContact';

export const appRoutes: AppRoute[] = [
	{
		path: '/contact',
		exact: true,
		content: Contacts
	},
	{
		path: '/create',
		exact: true,
		content: CreateContact
	}
];
