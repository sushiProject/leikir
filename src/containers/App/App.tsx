import React from 'react';
import ReactDOM from 'react-dom';
import { PageLayout } from '../pageLayout';
import { Container } from 'reactstrap';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './main.css';

export const App = () => {
	return (
		<BrowserRouter>
			<Switch>
				<Container>
					<Route component={PageLayout} />
				</Container>
			</Switch>
		</BrowserRouter>
	);
};
