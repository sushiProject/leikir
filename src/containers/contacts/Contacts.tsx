import * as React from 'react';

import { Button } from 'reactstrap';

export type propsContacts = {};
export type stateContacts = {};

export class Contacts extends React.Component<propsContacts, stateContacts> {
	render() {
		return <Button color="danger">Contacts</Button>;
	}
}
