import { MenuItem } from '../../types';

export const navigation: MenuItem[] = [
	{
		name: 'Gestion des contacts',
		path: '/contact'
	},
	{
		name: 'Create Contact',
		path: '/create'
	}
];
