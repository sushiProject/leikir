import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import { Contacts } from '../contacts';
import { NavBar } from '../../components/NavBar';
import { navigation } from './constants';
import { appRoutes } from '../../routes';

export class PageLayout extends React.Component {
	constructor(props: any) {
		super(props);
	}

	render() {
		return (
			<div>
				<NavBar menu={navigation} />
				<Switch>
					{appRoutes.map((route, index) => {
						return (
							<Route
								key={index}
								exact={route.exact}
								path={route.path}
								render={() => (
									<React.Fragment>
										<route.content route={route} />
									</React.Fragment>
								)}
							/>
						);
					})}
					<Route
						exact
						key="/"
						path="/"
						render={() => <Redirect to="/contact" />}
					/>
				</Switch>
			</div>
		);
	}
}
