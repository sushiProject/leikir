import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { MenuItem } from '../../types';

import './NavBar.css';

export type propsNavBar = {
	menu: MenuItem[];
};
export type statesNavBar = {};

export class NavBar extends React.Component<propsNavBar, statesNavBar> {
	constructor(props: propsNavBar) {
		super(props);
	}

	generateMenu(menu: MenuItem[]) {
		if (!menu.length) return null;

		return menu.map((item, index) => {
			return (
				<NavLink
					key={index}
					to={item.path}
					className="item"
					activeClassName="active"
				>
					{item.name}
				</NavLink>
			);
		});
	}

	render() {
		const { menu } = this.props;
		return <div>{this.generateMenu(menu)}</div>;
	}
}
