export type MenuItem = {
	name: string;
	path: string;
	routes?: MenuItem[];
};

export type AppRoute = {
	path: string;
	exact?: boolean;
	sectionName?: string;
	content: any;
};
