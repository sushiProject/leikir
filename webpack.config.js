const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	entry: {
		app: './src/index.tsx'
	},
	resolve: {
		modules: ['node_modules'],
		extensions: ['.ts', '.jsx', '.js', '.tsx', '.json'],
		plugins: [
			// Allow ts-loader to resolve modules according to baseUrl and paths in tsconfig.json
			new TsconfigPathsPlugin({
				configFile: path.join(__dirname, './tsconfig.json')
			})
		]
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				loader: ['babel-loader', 'ts-loader']
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: 'html-loader',
						options: { minimize: true }
					}
				]
			},
			{
				test: /\.css$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader']
			}
		]
	},
	devServer: {
		historyApiFallback: true
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: './src/template/index.html',
			filename: './index.html'
		}),
		new MiniCssExtractPlugin({
			filename: './src/[name].css',
			chunkFilename: '[id].css'
		})
	]
};
