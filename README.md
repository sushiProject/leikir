# Leikir

### Techno

```bash
React
Webpack 4
TypeScript
ES6
Babel 6
Reactstrap
```

### Requirements

```bash
// Install node 8.x via nvm
nvm install node 8
```

```bash
// Install npm 5.2 via npm
nvm install -g npm@5.2.0
```

```bash
// Install yarn via npm
nvm install -g yarn
```

### Config

```bash
// Install package via yarn
yarn
```

### Lunch Webpack-server-dev

```bash
yarn start
```
